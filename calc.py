#Calculadora

#Definimos las funciones sumar y restar

def sumar(n0: int, n1: int) -> int:
    return n0+n1

def restar(n0: int, n1:int) -> int:
    return n0-n1

print("1 + 2 =",sumar(1,2))
print("3 + 4 =", sumar(3,4))
print("5 - 6 =", restar(6,5))
print("7 - 8 =", restar(8,7))
